<?php
declare(strict_types=1);

class IndexController extends Phalcon\Mvc\Controller
{
    // проверка активной сессии
    public function indexAction()
    {
        $this->tag->setTitle('Авторизация');

        if ($this->session->has('IS_LOGIN')) {
            return $this->response->redirect('table');
        }
    }

    // авторизация
    public function loginAction()
    {
        try {
            if (!$this->request->isPost()) {
                return $this->response->redirect('index');
            }
         
            // поиск учетной записи в БД
            $consultant = Consultant::findFirst(array(
                'phone = :phone: and password = :password:', 'bind' => array(
                    'phone' => $this->request->getPost("login"),
                    'password' => md5($this->request->getPost("password"))
                )
            ));

            // если данные подошли
            if ($consultant) {
                $this->session->set('IS_LOGIN', 1);
                $this->session->set('USER_NAME', $consultant->surname . " " . $consultant->name);
                return $this->response->redirect('table');
            }

            // если данные не подошли
            $this->flash->error("Неверный логин/пароль");
            return $this->response->redirect('index');
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
    }

    // обработка ошибки 404
    public function error404Action()
    {
        $this->tag->setTitle('Ошибка 404');
    }

    // обработка ошибки 503
    public function error503Action()
    {
        $this->tag->setTitle('Ошибка 503');
    }

    // функция для модульного теста
    public function sumUnitTest($first, $second)
    {
        return $first + $second;
    }
}
