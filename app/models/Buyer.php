<?php

class Buyer extends \Phalcon\Mvc\Model
{
    public $idBuyer;
    public $name;
    public $surname;
    public $patronymic;
    public $passportSeries;
    public $passportNumber;
    public $city;
    public $street;
    public $house;
    public $flat;
    public $phone;

    // связь один ко многим с таблицей Sale
    public function initialize()
    {
        $this->hasMany("idBuyer", "Sale", "idBuyer");
    }
}
