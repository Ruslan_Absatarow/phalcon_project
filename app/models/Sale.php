<?php

class Sale extends \Phalcon\Mvc\Model
{
    public $idSale;
    public $idBuyer;
    public $date;
    public $idCar;
    public $colour;
    public $price;
    public $idConsultant;

    // связь многие к одному
    public function initialize()
    {
        // с таблицей Buyer
        $this->belongsTo("idBuyer", "Buyer", "idBuyer");
        // с таблицей Car
        $this->belongsTo("idCar", "Car", "idCar");
        // с таблицей Consultant
        $this->belongsTo("idConsultant", "Consultant", "idConsultant");
    }
}
